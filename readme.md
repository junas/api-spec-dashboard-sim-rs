# API SPEC
## Create Task
Request
- Method: POST
- Endpoint: `/api/v1/tasks`
- Header:
    - Content-Type : application/json
    - Accept : application/json
- Body
```json
{
    "id": "number",
    "taskName" : "string",
    "description": "string",
    "status" : "boolean",
    "createAt" : "timestamp"
}
```
- Response 
```json
{
    "code" : "number",
    "status" : "boolean",
    "data": "string"
}
```
## Get Task
Request
- Method: GET
- Endpoint: `/api/v1/tasks/{id}`
- Header:
    - Accept : application/json
- Response 
```json
{
    "code" : "number",
    "status" : "boolean",
    "data": {
        "id": "number",
        "taskName" : "string",
        "description": "string",
        "status" : "boolean",
        "createAt" : "timestamp",
        "updateAt" :"timestamp"
    }
}
```
## Update Task
Request
- Method: PUT
- Endpoint: `/api/v1/tasks/{id}`
- Header:
    - Content-Type : application/json
    - Accept : application/json
- Body
```json
{
    "taskName" : "string",
    "description": "string",
    "status" : "boolean"
}
```
- Response 
```json
{
    "code" : "number",
    "status" : "boolean",
    "data": "string"
}
```
## List Task
Request
- Method: GET
- Endpoint: `/api/v1/tasks`
- Header:
    - Accept : application/json
- Query Param
    - page : number,
    - pageSize : number
- Response 
```json
{
    "code" : "number",
    "status" : "boolean",
    "data": [
        {
            "id": "number",
            "taskName" : "string",
            "description": "string",
            "status" : "boolean",
            "createAt" : "timestamp",
            "updateAt" :"timestamp"
        },
        {
            "id": "number",
            "taskName" : "string",
            "description": "string",
            "status" : "boolean",
            "createAt" : "timestamp",
            "updateAt" :"timestamp"
        }
    ]
}
```
## Delete Task
Request
- Method: DELETE
- Endpoint: `/api/v1/tasks`
- Header:
    - Accept : application/json
- Body 
```json
{
    "id":"number"
}
```
- Response 
```json
{
    "code" : "number",
    "status" : "boolean",
    "data":"string"
}
```